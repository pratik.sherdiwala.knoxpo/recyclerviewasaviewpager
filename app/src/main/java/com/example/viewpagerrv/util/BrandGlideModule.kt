package com.example.viewpagerrv.util

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class BrandGlideModule : AppGlideModule()