package com.example.viewpagerrv.ui.brandlist

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.viewpagerrv.R
import com.example.viewpagerrv.databinding.ActivityBrandListBinding
import com.example.viewpagerrv.ui._common.DataBindingActivity
import com.example.viewpagerrv.ui.brandlist.adapter.BrandListAdapter
import dagger.android.AndroidInjection
import javax.inject.Inject

class BrandListActivity : DataBindingActivity<ActivityBrandListBinding>() {

    override val layoutID = R.layout.activity_brand_list

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
            .get(BrandListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity

            viewModel = activity.viewModel

            brandListRV.layoutManager = LinearLayoutManager(activity)
            brandListRV.adapter = BrandListAdapter()
        }

        viewModel.getBrandList()
    }
}