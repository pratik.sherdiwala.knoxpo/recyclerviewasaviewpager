package com.example.viewpagerrv.ui.brandlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpagerrv.R
import com.example.viewpagerrv.model.BrandResponse
import com.example.viewpagerrv.ui.brandlist.adapter.viewholder.BrandVH

class BrandListAdapter : RecyclerView.Adapter<BrandVH>() {

    private var brands: List<BrandResponse>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrandVH {
        return BrandVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_brand,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = brands?.size ?: 0

    override fun onBindViewHolder(holder: BrandVH, position: Int) {
        holder.bindBrand(brands!![position])
    }

    fun updateBrand(newItems: List<BrandResponse>) {
        brands = newItems
        notifyDataSetChanged()
    }
}