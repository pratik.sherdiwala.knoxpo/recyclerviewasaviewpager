package com.example.viewpagerrv.ui.brandlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpagerrv.R
import com.example.viewpagerrv.ui.brandlist.adapter.viewholder.ImageVH

private const val ITEM_IMAGE = 2

class ImageAdapter(
    private var urls: List<String>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        when (viewType) {

            ITEM_IMAGE -> {
                return ImageVH(
                    LayoutInflater.from(
                        parent.context
                    ).inflate(
                        R.layout.item_image,
                        parent,
                        false
                    )
                )
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return ITEM_IMAGE
    }

    override fun getItemCount() = urls.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ImageVH -> holder.bindImage(urls[position])
        }
    }

    fun updateImage(newImages: List<String>) {
        urls = newImages
        notifyDataSetChanged()
    }
}