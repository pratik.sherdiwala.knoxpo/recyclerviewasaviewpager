package com.example.viewpagerrv.ui.brandlist.adapter.viewholder

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpagerrv.R
import com.example.viewpagerrv.model.BrandResponse
import com.example.viewpagerrv.ui.brandlist.adapter.ImageAdapter
import com.example.viewpagerrv.util.GlideApp


class BrandVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mBrandNameTV = itemView.findViewById<TextView>(R.id.brandNameTV)
    private val mBrandLogoIV = itemView.findViewById<ImageView>(R.id.brandLogoIV)
    private val mBrandImagesRV = itemView.findViewById<RecyclerView>(R.id.imagesRV)
    private val mDotsLayout = itemView.findViewById<LinearLayout>(R.id.imageDotsLL)
    private val mRightArrowIV = itemView.findViewById<ImageView>(R.id.rightArrowIV)
    private val mLeftArrowIV = itemView.findViewById<ImageView>(R.id.leftArrowIV)

    private var snapPosition = RecyclerView.NO_POSITION


    var dots: ArrayList<ImageView>? = null

    fun bindBrand(
        brandResponse: BrandResponse
    ) {

        mBrandNameTV.text = brandResponse.brandName

        if (brandResponse.images.isNullOrEmpty()) {
            mRightArrowIV.isVisible = false
            mLeftArrowIV.isVisible = false
        } else {
            mLeftArrowIV.isVisible = true
            mRightArrowIV.isVisible = true
        }

        brandResponse.images?.let {
            addDots(it.size)
        }

        GlideApp.with(itemView)
            .load(brandResponse.brandLogo)
            .centerCrop()
            .circleCrop()
            .into(mBrandLogoIV)

        mBrandImagesRV.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        mBrandImagesRV.adapter = ImageAdapter(brandResponse.images ?: listOf(brandResponse.brandLogo))
        mBrandImagesRV.onFlingListener = null

        mDotsLayout.removeAllViews()

        mLeftArrowIV.setOnClickListener {
            mBrandImagesRV.scrollToPosition(snapPosition-1)
        }

        mRightArrowIV.setOnClickListener {
            mBrandImagesRV.scrollToPosition(snapPosition+1)
        }

        if (mBrandImagesRV.onFlingListener == null) {
            val snapHelper = PagerSnapHelper()
            snapHelper.attachToRecyclerView(mBrandImagesRV)
            brandResponse.images?.let {
                addDots(it.size)
                mBrandImagesRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        mayBeNotifySnapPositionChanged(recyclerView, snapHelper, it)
                    }
                })
            }
        }
    }

    private fun mayBeNotifySnapPositionChanged(
        recyclerView: RecyclerView,
        snapHelper: PagerSnapHelper,
        list: List<String>
    ) {
        val layoutManager = recyclerView.layoutManager
        val snapView = snapHelper.findSnapView(layoutManager)
        snapPosition = layoutManager!!.getPosition(snapView!!)

        if (snapPosition == 0) {
            mLeftArrowIV.setColorFilter(Color.GRAY)
        } else {
            mLeftArrowIV.setColorFilter(Color.BLACK)
        }

        if (snapPosition == list.size - 1) {
            mRightArrowIV.setColorFilter(Color.GRAY)
        } else {
            mRightArrowIV.setColorFilter(Color.BLACK)
        }

        selectedDot(list.size, snapPosition)
    }

    private fun addDots(images: Int) {
        dots = ArrayList()

        for (i in 0 until images) {
            val image = ImageView(itemView.context)
            image.setImageDrawable(itemView.context.resources.getDrawable(R.drawable.ic_dot_selected))
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(5, 0, 5, 0)
            dots?.add(image)
            mDotsLayout.addView(image, params)
        }
    }

    private fun selectedDot(images: Int, index: Int) {
        val res = itemView.context.resources

        for (i in 0 until images) {
            val drawableId = if (i == index) {
                R.drawable.ic_dot_selected
            } else
                R.drawable.ic_dot_unselected

            val drawable = res.getDrawable(drawableId)
            dots?.get(i)?.setImageDrawable(drawable)
        }
    }
}