package com.example.viewpagerrv.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpagerrv.model.Brand
import com.example.viewpagerrv.model.BrandResponse
import com.example.viewpagerrv.ui.brandlist.adapter.BrandListAdapter
import com.example.viewpagerrv.ui.brandlist.adapter.ImageAdapter

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("brand")
    fun setBrands(view: RecyclerView, brands: List<BrandResponse>?) {
        brands?.let {
            (view.adapter as BrandListAdapter).updateBrand(it)
        }
    }

    @JvmStatic
    @BindingAdapter("image")
    fun setImages(view:RecyclerView,images: List<String>?){
        images?.let {
            (view.adapter as ImageAdapter).updateImage(it)
        }
    }
}