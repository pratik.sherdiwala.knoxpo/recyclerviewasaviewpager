package com.example.viewpagerrv.ui.brandlist

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.viewpagerrv.data.network.repository.BrandRepository
import com.example.viewpagerrv.model.Brand
import com.example.viewpagerrv.model.BrandResponse
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


private val TAG=BrandListViewModel::class.java.simpleName

class BrandListViewModel @Inject constructor(
    private val brandRepository: BrandRepository
) : ViewModel() {

    var brandList = MutableLiveData<List<BrandResponse>>()

    private val disposables = CompositeDisposable()

    fun getBrandList() {
        brandRepository.getBrandDetails()
            .subscribe(
                {
                    brandList.value = it.brandResponse
                },
                {
                    Log.e(TAG, "Error while fetching brandList ", it)
                }
            ).also {
                disposables.add(it)
            }
    }

    fun rightClicked(){
        Log.e(TAG,"Right Clicked")
    }

    fun leftClicked(){
        Log.e(TAG,"Left Clicked")
    }
}