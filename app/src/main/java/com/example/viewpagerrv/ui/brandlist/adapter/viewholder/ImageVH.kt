package com.example.viewpagerrv.ui.brandlist.adapter.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpagerrv.R
import com.example.viewpagerrv.util.GlideApp

class ImageVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mBrandImageRV = itemView.findViewById<ImageView>(R.id.brandImage)

    fun bindImage(image: String) {
        GlideApp.with(itemView)
            .load(image)
            .centerCrop()
            .into(mBrandImageRV)
    }
}