package com.example.viewpagerrv.data.network.repository

import com.example.viewpagerrv.data.network.BrandApi
import com.example.viewpagerrv.model.Brand
import com.example.viewpagerrv.model.BrandResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

private const val CATEGORY = "ag9ifmdsb2JhbGNpdHktMjByFQsSCENhdGVnb3J5GICAgID62YMKDA"
private const val AREA_ID = "ag9ifmdsb2JhbGNpdHktMjByEQsSBEFyZWEYgICAgM6cggoM"

class BrandRepository @Inject constructor(
    private val brandApi: BrandApi
) {
    fun getBrandDetails(): Single<Brand> {
        return brandApi.getBrandDetails(CATEGORY, AREA_ID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}