package com.example.viewpagerrv.data.network

import com.example.viewpagerrv.model.Brand
import com.example.viewpagerrv.model.BrandResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BrandApi {
    @GET("brand")
    fun getBrandDetails(
        @Query("category") category: String,
        @Query("areaId") area: String
    ): Single<Brand>
}