package com.example.viewpagerrv.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.viewpagerrv.ui.brandlist.BrandListViewModel
import com.example.viewpagerrv.viewmodel.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(BrandListViewModel::class)
    abstract fun bindBrandListViewModel(viewModel: BrandListViewModel):ViewModel

    @Binds
    abstract fun provideAppviewModelFactory(appViewModelFactory: AppViewModelFactory):ViewModelProvider.Factory

}