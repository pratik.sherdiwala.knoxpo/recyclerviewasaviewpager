package com.example.viewpagerrv.di.module

import com.example.viewpagerrv.ui.brandlist.BrandListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BrandListActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeBrandListActivity(): BrandListActivity

}