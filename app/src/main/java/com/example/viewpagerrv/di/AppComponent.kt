package com.example.viewpagerrv.di

import com.example.viewpagerrv.App
import com.example.viewpagerrv.di.module.BrandListActivityModule
import com.example.viewpagerrv.di.module.NetworkModule
import com.example.viewpagerrv.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        BrandListActivityModule::class,
        ViewModelModule::class,
        NetworkModule::class
    ]
)

interface AppComponent {

    fun inject(app: App)

}