package com.example.viewpagerrv.di.module

import com.example.viewpagerrv.data.network.BrandApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://globalcity-20.appspot.com/api/v1/")
            .build()
    }

    @Provides
    fun provideBrandApi(retrofit: Retrofit): BrandApi {
        return retrofit.create(BrandApi::class.java)
    }

}