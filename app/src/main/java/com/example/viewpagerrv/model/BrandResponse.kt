package com.example.viewpagerrv.model

data class BrandResponse(
    val brandName: String,
    val brandAddress: String,
    val brandLogo: String,
    val images: List<String>?
)