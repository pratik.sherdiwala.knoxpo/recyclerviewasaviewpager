package com.example.viewpagerrv.model

import com.google.gson.annotations.SerializedName

data class Brand(
    @SerializedName("brands") val brandResponse: List<BrandResponse>,
    val count: String
)